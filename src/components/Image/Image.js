import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      style: "filter",
      filterIndex: 1,
      liked: ""
    };
  }

  //calc image size function (dynamic)
  calcImageSize() {
    const targetSize = 200;
    const imagesPerRow = Math.round(this.props.galleryWidth / targetSize);
    const size = (this.props.galleryWidth / imagesPerRow);
    this.setState({ size: size - 3 });
  }
 
  componentWillReceiveProps(props) {
    this.calcImageSize();
  }
 
  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  //image filter function
  onClickFilter() {
    let num = this.randNum();
    const { filterIndex } = this.state;

    while (num === filterIndex)
      num = this.randNum();

    this.setState({ filterIndex: num });
    this.setState({ style: "filter" + this.state.filterIndex });
  }

  //call urlFromDto function when props defined.
  imageUrl() {
    return this.props ? this.urlFromDto(this.props.dto) : undefined;
  }

  //random number
  randNum() {
    return Math.floor(Math.random() * Math.floor(5)) + 1;
  }

  //sending image url up 
  onExpand = () => {
    this.props.onExpand(this.imageUrl());
  }

  //like function 
  onClickLike = () => {
    let { liked } = this.state;
    liked ? this.setState({ liked: "" }) : this.setState({ liked: "liked" });
  }

  render() {
   
    return (
      <div 
        className={`image-root ${this.state.style}`}
        style={{
          backgroundImage: `url(${this.imageUrl()})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
      >

        <div id="center-icons"> 
          <FontAwesome onClick={this.props.clickClone} className="image-icon-center" name="clone" title="Dupplicate" />
          <FontAwesome onClick={()=>this.onClickFilter()} className="image-icon-center" name="filter" title="Filter" />
          <FontAwesome onClick={this.onExpand} className="image-icon-center" name="expand" title="Expand" />
        </div>
        <div id="bottom-icons"> 
          <FontAwesome onClick={this.onClickLike} className={`image-icon-bottom ${this.state.liked}`} name="heart" title="Like" />
          <FontAwesome onClick={this.props.clickDelete} className="image-icon-bottom" name="trash-alt" title="Delete" />
        </div>
      </div>
    );
  }
}

export default Image;


