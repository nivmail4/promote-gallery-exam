import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';


const timeTrashold = 3000; // should be 500ms but doesnt work well. 
let lastTime = 0;

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      page: 1,
      galleryWidth: 1000
    };
  }

  //get images function, not perfectly done. 
  getImages(tag) {
    if (tag.length === 1) {
      this.setState({ images: [] });
      this.setState({ page: 1 });
    }

    else {
      const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=50&page=${this.state.page}&format=json&safe_search=1&nojsoncallback=1`;
      const baseUrl = 'https://api.flickr.com/';
      this.setState({})
      axios({
        url: getImagesUrl,
        baseURL: baseUrl,
        method: 'GET'
      })
        .then(res => res.data)
        .then(res => {
          if (
            res &&
            res.photos &&
            res.photos.photo &&
            res.photos.photo.length > 0
          ) {
            this.setState({
              images: this.state.images.concat(res.photos.photo),
              page: this.state.page + 1
            });
          }
        });
    }
  }

  getGalleryWidth() {
//  console.log("get galleryWidth Excuted");
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  //window size is updated=>
  updateDimensions() {
    if (window.innerWidth >= 500)
      this.setState({ galleryWidth: document.body.clientWidth });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll, false);
    let width = this.getGalleryWidth();
    this.setState({ galleryWidth: width });
    this.getImages(this.props.tag);

    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  componentWillUnmount() {
    //scroll listener
    window.removeEventListener('scroll', this.onScroll, false);
    //window resize listener
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  componentWillReceiveProps() {
    if (lastTime === 0) { //first time
      lastTime = new Date().getTime();
      console.log(lastTime);
      this.getImages(this.props.tag);
    }
    else { 
      let tempTime = new Date().getTime();
      const diff = tempTime - lastTime;
      if (diff > timeTrashold) { //timer is up and should send api request
        this.getImages(this.props.tag);
      }
    }
  }

  //on scroll function execute 
  onScroll = () => {
    if ( (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100) ) {
      this.getImages(this.props.tag);
    }
  }

  //dupplicate image
  onClickClone = (imageIndex) => {
    const newImages = this.state.images.slice();
    newImages.push(newImages[imageIndex])  ;
    this.setState({ images: newImages });
  }

  //delete image
  onClickDelete = (imageIndex) => {
    const newImages = this.state.images.slice();
    newImages.splice(imageIndex, 1);
    this.setState({ images: newImages });
  }

  render() {
    return (
      <div className="gallery-root" >
        {this.state.images.map((dto,index) => {
          return <Image
            key={'image-' + index}
            clickClone={() => this.onClickClone(index)}
            clickDelete={() => this.onClickDelete(index)}
            dto={dto}
            onExpand={this.props.onExpand}
            galleryWidth={this.state.galleryWidth} />;
        })}
      </div>
    );
  }
}

export default Gallery;
