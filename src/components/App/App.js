import React from 'react';
import './App.scss';
import Gallery from '../Gallery';


class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'Art',
      imageUrl: ""
    };
  }

  //necessary to get first images 
  componentWillMount() {
    this.forceUpdate();
  }

  //new input to be update
  eventHandler = event => {
    this.setState({ tag: event.target.value });
  }

  //expand image function
  onExpand = imageUrl => {
    this.setState({ imageUrl });
  }

  //close expanded image function
  onExpandClose = () => {
    this.setState({ imageUrl: "" });
  }

  render() {
    const { imageUrl } = this.state;
 
    const overlay = <div className="overlay">
      <div className="background-layer"></div>
      <div className="content-layer">

        <button className="close-button" onClick={this.onExpandClose}>X </button>
        <img className="image" src={imageUrl} />
      </div>
    </div>;

    return (

      <div className="app-root">
        {
          imageUrl ? overlay : null
        }
        <div className="app-header">
          <p> </p>
          <h2>FLICKR GALLERY</h2>
          <input className="app-input" onChange={this.eventHandler} value={this.state.tag} />
        </div>
        <Gallery onExpand={this.onExpand} tag={this.state.tag} />
      </div>
    );
  }
}

export default App;
